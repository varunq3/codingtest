from rest_framework import routers
from .api import CustomUserAPIView, \
    CustomerAPIView, PolicyAPIView

democrance_router = routers.DefaultRouter()
democrance_router.register(
    r'create_customer', CustomerAPIView,
    basename='customer')

democrance_router.register(
    r'create_policy', PolicyAPIView,
    basename='policy')

democrance_router.register(
    r'customusers', CustomUserAPIView,
    basename='customusers')
