from rest_framework import serializers
from ..models import CustomUsers, Customer, Policy


class CustomUserSerializer(serializers.HyperlinkedModelSerializer):
    role = serializers.SerializerMethodField()

    def get_role(self,obj):
        return obj.get_role_display()

    class Meta:
        model = CustomUsers
        fields = ('id', 'name', 'username', 'email',
                  'mobile', 'date_joined', 'role')


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields = ('url','id', 'first_name',
                  'last_name', 'dob',
                  'created_by_id')


class PolicySerializer(serializers.HyperlinkedModelSerializer):
    customer = CustomerSerializer(read_only=True)
    customer_id = serializers.PrimaryKeyRelatedField(
        queryset=Customer.objects.all(),
        read_only=False)

    def create(self, validated_data):
        from django.conf import settings
        customer = validated_data.pop('customer_id')
        additional_data = {'customer':customer}
        if settings.USE_AUTHENTICATION:
            additional_data['agent'] = self.context.get('request').user

        policy = Policy.objects.create(
            **additional_data,
            **validated_data)
        return policy

    class Meta:
        model = Policy
        fields = (
            'url', 'customer',
            'customer_id', 'agent_id','type',
            'premium', 'cover')
