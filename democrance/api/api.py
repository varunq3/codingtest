from .serializers import \
    CustomUserSerializer, CustomerSerializer, \
    PolicySerializer, CustomUsers, \
    Customer, Policy

from rest_framework import viewsets

from django.conf import settings


class CustomerAPIView(viewsets.ModelViewSet):
    serializer_class = CustomerSerializer

    def get_queryset(self):
        if settings.USE_AUTHENTICATION:
            if self.request.user.role == 2:
                queryset = Customer.objects.all()
            else:
                queryset = Customer.objects.filter(created_by=self.request.user)
        else:
            queryset = Customer.objects.all()
        return queryset


class PolicyAPIView(viewsets.ModelViewSet):
    serializer_class = PolicySerializer

    def get_queryset(self):
        if settings.USE_AUTHENTICATION and self.request.user.role != 3:
            if self.request.user.role == 2:
                queryset = Policy.objects.all()
            else:
                queryset = Policy.objects.filter(agent=self.request.user)
        else:
            queryset = Policy.objects.all()
        return queryset


class CustomUserAPIView(viewsets.ModelViewSet):
    serializer_class = CustomUserSerializer

    def get_queryset(self):
        if settings.USE_AUTHENTICATION:
            if self.request.user.role == 2:
                queryset = CustomUsers.objects.all()
            else:
                queryset = CustomUsers.objects.filter(id=self.request.user.id)
        else:
            queryset = CustomUsers.objects.all()
        return queryset
