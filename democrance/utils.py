from datetime import date
from django.core.exceptions import ValidationError


def restrict_future_date(value):
    today = date.today()
    if value > today:
        raise ValidationError('Date of birth cannot be in the future.')
