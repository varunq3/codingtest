from django.contrib import admin
from .models import Customer, CustomUsers, Policy


# Register your models here.
class CustomUsersAdmin(admin.ModelAdmin):
    list_display = ('username', 'name',
                    'email', 'mobile',
                    'role', 'is_staff','is_superuser',
                    'is_active')
    fields = ('username', 'name',
              'email', 'mobile',
              'role', 'is_staff',
              'is_active','password',
              'is_superuser')


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name',
                    'dob')


class PolicyAdmin(admin.ModelAdmin):
    list_display = ('name', 'agent_name',
                    'type', 'premium', 'cover')

    def name(self, obj):
        return "{} {}".format(obj.customer.first_name,
                              obj.customer.last_name
                              if obj.customer.last_name
                              else "")

    def agent_name(self, obj):
        return "{} | {}".format(obj.agent.name,
                                obj.agent.username) if obj.agent else ""


admin.site.register(CustomUsers, CustomUsersAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Policy, PolicyAdmin)
