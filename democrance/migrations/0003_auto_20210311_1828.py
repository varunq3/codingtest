# Generated by Django 3.1.7 on 2021-03-11 18:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('democrance', '0002_auto_20210311_1825'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customusers',
            name='mobile',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
    ]
