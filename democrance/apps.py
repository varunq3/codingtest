from django.apps import AppConfig


class DemocranceConfig(AppConfig):
    name = 'democrance'
