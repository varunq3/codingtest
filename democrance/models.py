from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from .manager import UsersManager
from .picklist import USER_ROLES
from django.utils.translation import ugettext_lazy as _
from .utils import restrict_future_date

from django_currentuser.db.models import CurrentUserField


class CustomUsers(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        unique=True,
        max_length=50)
    email = models.EmailField(_('email address'), unique=True,
                              blank=True,
                              null=True)
    mobile = models.CharField(max_length=10, blank=True, null=True)
    name = models.CharField(max_length=100,
                            blank=True,
                            null=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    role = models.IntegerField(choices=USER_ROLES,
                               blank=True, null=True)
    objects = UsersManager()

    USERNAME_FIELD = 'username'

    class Meta:
        verbose_name = _('Custom User')
        verbose_name_plural = _('Custom User(s)')


class Customer(models.Model):
    created_by = CurrentUserField()
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50,
                                 blank=True,
                                 null=True)
    dob = models.DateField(validators=[restrict_future_date])

    def __str__(self):
        return "{} {}".format(self.first_name,
                              self.last_name if self.last_name else "")


class Policy(models.Model):
    customer = models.ForeignKey(Customer,
                                 related_name="policy_holder",
                                 on_delete=models.CASCADE)
    agent = models.ForeignKey(CustomUsers,
                              related_name="policy_agent",
                              on_delete=models.CASCADE, blank=True, null=True)
    type = models.CharField(max_length=50)
    premium = models.PositiveIntegerField()
    cover = models.PositiveIntegerField()
